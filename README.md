# GeoQuiz

**The offline quiz-app for geography enthusiasts.**<br>
*Test your knowledge!*

**"GeoQuiz"** is a quiz game that is not about speed. No, pure knowledge is more important. You can consolidate your knowledge, but also learn new things!<br>
In addition, the app does not require an internet connection and is therefore perfectly suited for mobile or travel activities.
<br>
**Get it for free on Google Play:** *https://play.google.com/store/apps/details?id=mosmueller.geoquiz*
<br><br>

## Content
There are **seven different categories** in which you can prove your skills:

#### 1.) Detect flags
Out of over 200 flags – from countries all around the world – a randomly picked one is shown and you get four possible answers.
<br>Do you recognize the flag's country?

#### 2.) Identify countries
A world map is shown on which a randomly picked country of this earth is colored.
<br>But which one? Give the correct answer!

#### 3.) Facts
Pure knowledge is needed here.
<br>Questions from the most diverse areas of geography are asked. Do you know the solution?

#### 4.) US states
A map of the USA is shown with one of the states colored.
<br>Determine which one.

#### 5.) US capitals
A map of the USA is shown with one of the states colored.
<br>But what's the states capital?

#### 6.) US states nicknames
A map of the USA is shown with one of the states colored.
<br>You'll then get four possible nicknames for the state. Which one is correct?

#### 7.) Daily-Challenge
Would you like a challenge?
<br>You can solve ten of these tasks daily. But beware, it won't be easy: this challenge uses questions from all categories (1–6), and mixes these motley.
<br>But there are also more points 😜

Speaking of points:<br>
Earn as many points as possible and unlock all ten levels in turn!