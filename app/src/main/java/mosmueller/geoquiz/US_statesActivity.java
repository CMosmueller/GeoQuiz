package mosmueller.geoquiz;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.util.Random;

import static java.lang.String.valueOf;

public class US_statesActivity extends AppCompatActivity {

    String arr[][] = new String[4][3];
    Random rand = new Random();
    int i;
    boolean alr = false;
    String prev;
    Menu mainMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_us_states);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_US_statesActivity));

        setOnClicks();
        readTask();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.game_menu, menu);
        mainMenu = menu;

        SharedPreferences prefs = getSharedPreferences("score", MODE_PRIVATE);
        int points = prefs.getInt("us_score", 0);
        MenuItem settingsMenuItem = mainMenu.findItem(R.id.game_score);
        settingsMenuItem.setTitle(valueOf(points) + " P");

        return true;
    }

    public void readTask() {

        // Reset array
        arr[0][0] = null;
        arr[1][0] = null;
        arr[2][0] = null;
        arr[3][0] = null;

        Database db = new Database(this);
        SQLiteDatabase sqldb = db.openDatabase();

        Cursor c;
        for(int four = 0; four<4; four++)
        {
            c = db.readS(sqldb);

            if(c != null && c.getCount() > 0) {
                c.moveToFirst();
                //Log.d("DB","Eintrag vorhanden: " + c.getString(0) + " " + c.getString(1) + " " + c.getString(2));

                String checkID = c.getString(0);

                if(arr[0][0]==null)
                {
                    arr[0][0]=c.getString(0);
                    arr[0][1]=c.getString(1);
                    arr[0][2]=c.getString(2);
                }
                else if(arr[1][0]==null && !arr[0][0].equals(checkID))
                {
                    arr[1][0]=c.getString(0);
                    arr[1][1]=c.getString(1);
                    arr[1][2]=c.getString(2);
                }
                else if(arr[2][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID))
                {
                    arr[2][0]=c.getString(0);
                    arr[2][1]=c.getString(1);
                    arr[2][2]=c.getString(2);
                }
                else if(arr[3][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID) && !arr[2][0].equals(checkID))
                {
                    arr[3][0]=c.getString(0);
                    arr[3][1]=c.getString(1);
                    arr[3][2]=c.getString(2);
                }
                else{
                    four--;
                }
            }
            else { // ERROR!
                Log.d("DB","Kein Eintrag vorhanden!");
            }

            if(four==3)
            {
                c.close();
            }
        }

        sqldb.close();
        showTask();
    }

    public void showTask() {

        // Get random correct answer
        i = rand.nextInt(4);
        //Log.d("show","Random number: " + i);

        if(prev != null) {
            while(prev.equals(arr[i][0])){
                i = rand.nextInt(4);
                //Log.d("show","New random number: " + i);
            }
        }

        prev =  arr[i][0];

        String image = "state_" + arr[i][2];
        image = image.toLowerCase();


        // show SVG
        ImageView imageView = (ImageView) findViewById(R.id.us_states_ImageView);
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        try {
            // Build Resource
            Resources res = this.getResources();
            int resID = res.getIdentifier(image, "raw", this.getPackageName());

            // Show
            SVG svg = SVG.getFromResource(this, resID);
            Drawable drawable = new PictureDrawable(svg.renderToPicture());
            imageView.setImageDrawable(drawable);
        }
        catch(SVGParseException e){}

        TextView tv1 = (TextView)findViewById(R.id.q1);
        tv1.setText(arr[0][1]);
        TextView tv2 = (TextView)findViewById(R.id.q2);
        tv2.setText(arr[1][1]);
        TextView tv3 = (TextView)findViewById(R.id.q3);
        tv3.setText(arr[2][1]);
        TextView tv4 = (TextView)findViewById(R.id.q4);
        tv4.setText(arr[3][1]);

    }

    public void checkAnswer(View view) {
        if(alr) {
            return;
        }
        alr = true;

        // set correct one
        ColorDrawable[] corrColor = {new ColorDrawable(getResources().getColor(R.color.color_us_states)), new ColorDrawable(getResources().getColor(R.color.colorCorrect))};
        TransitionDrawable corrTrans = new TransitionDrawable(corrColor);
        ColorDrawable[] incorrColor = {new ColorDrawable(getResources().getColor(R.color.color_us_states)), new ColorDrawable(getResources().getColor(R.color.colorIncorrect))};
        TransitionDrawable incorrTrans = new TransitionDrawable(incorrColor);


        Resources res = this.getResources();
        int resID = res.getIdentifier("linearAnswer" + valueOf(i+1), "id", this.getPackageName());
        LinearLayout corrLL = (LinearLayout) findViewById(resID);
        corrLL.setBackground(corrTrans);

        // set variables
        LinearLayout wrongLL;
        Toast pointToast;
        LinearLayout toastLayout;
        TextView toastTV;
        SharedPreferences prefs;
        SharedPreferences.Editor editor;
        int points;
        MenuItem settingsMenuItem;

        prefs = getSharedPreferences("score", MODE_PRIVATE);
        int games = prefs.getInt("us_totalGames", 0);
        games++;

        prefs = getSharedPreferences("score", MODE_PRIVATE);
        editor = prefs.edit();
        editor.putInt("us_totalGames", games);
        editor.commit();


        // do the checking
        int corrID = res.getIdentifier("answer" + valueOf(i+1), "id", this.getPackageName());

        if(view.getId() == corrID)
        {
            corrTrans.startTransition(50);

            pointToast = Toast.makeText(this, "+1P", Toast.LENGTH_SHORT);
            toastLayout = (LinearLayout) pointToast.getView();
            toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(17);
            toastTV.setTypeface(null, Typeface.BOLD);
            pointToast.setGravity(Gravity.TOP|Gravity.RIGHT,15,15);
            pointToast.show();

            // add score
            prefs = getSharedPreferences("score", MODE_PRIVATE);
            points = prefs.getInt("us_score", 0);
            points++;

            prefs = getSharedPreferences("score", MODE_PRIVATE);
            editor = prefs.edit();
            editor.putInt("us_score", points);
            editor.commit();

            settingsMenuItem = mainMenu.findItem(R.id.game_score);
            settingsMenuItem.setTitle(valueOf(points) + " P");
        }
        else if(view.getId() == R.id.answer1){
            wrongLL = (LinearLayout)findViewById(R.id.linearAnswer1);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }
        else if(view.getId() == R.id.answer2){
            wrongLL = (LinearLayout)findViewById(R.id.linearAnswer2);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }
        else if(view.getId() == R.id.answer3){
            wrongLL = (LinearLayout)findViewById(R.id.linearAnswer3);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }
        else if(view.getId() == R.id.answer4){
            wrongLL = (LinearLayout)findViewById(R.id.linearAnswer4);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                LinearLayout reset = (LinearLayout)findViewById(R.id.linearAnswer1);
                reset.setBackgroundColor(getResources().getColor(R.color.color_us_states));
                reset = (LinearLayout)findViewById(R.id.linearAnswer2);
                reset.setBackgroundColor(getResources().getColor(R.color.color_us_states));
                reset = (LinearLayout)findViewById(R.id.linearAnswer3);
                reset.setBackgroundColor(getResources().getColor(R.color.color_us_states));
                reset = (LinearLayout)findViewById(R.id.linearAnswer4);
                reset.setBackgroundColor(getResources().getColor(R.color.color_us_states));
                alr = false;
                readTask();
            }
        }, 1200);
    }

    public void setOnClicks()
    {
        View allViews = findViewById(R.id.answer1);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        allViews = findViewById(R.id.answer2);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        allViews = findViewById(R.id.answer3);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        allViews = findViewById(R.id.answer4);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
    }
}
