package mosmueller.geoquiz;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import static java.lang.String.valueOf;

public class FactsActivity extends AppCompatActivity {

    String arr[] = new String[5];
    boolean alr = false;
    String prev;
    Menu mainMenu;
    int corr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facts);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_FactsActivity));

        setOnClicks();
        readTask();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.game_menu, menu);
        mainMenu = menu;

        SharedPreferences prefs = getSharedPreferences("score", MODE_PRIVATE);
        int points = prefs.getInt("facts_score", 0);
        MenuItem settingsMenuItem = mainMenu.findItem(R.id.game_score);
        settingsMenuItem.setTitle(valueOf(points) + " P");

        return true;
    }

    public void readTask() {

        Database db = new Database(this);
        SQLiteDatabase sqldb = db.openDatabase();

        Cursor c = db.readQ(sqldb);

            if(c != null && c.getCount() > 0) {
                c.moveToFirst();
                //Log.d("DB","Eintrag vorhanden: " + c.getString(0) + " " + c.getString(1) + " " + c.getString(2) + " " + c.getString(3) + " " + c.getString(4));

                Boolean gotFact = false;
                while(!gotFact)
                {
                    if(!c.getString(0).equals(prev))
                    {
                        arr[0]=c.getString(0);
                        arr[1]=c.getString(1);
                        arr[2]=c.getString(2);
                        arr[3]=c.getString(3);
                        arr[4]=c.getString(4);

                        prev = c.getString(0);
                        gotFact = true;
                    }
                    else {
                        c = db.readQ(sqldb);
                        c.moveToFirst();
                    }
                }
            }
            else { // ERROR!
                Log.d("DB","Kein Eintrag vorhanden!");
            }

        c.close();
        sqldb.close();
        showTask();
    }

    public void showTask() {
        // Get random order
        Random rand = new Random();
        int i[] = new int[4];
        i[0] = rand.nextInt(4);
        i[1] = rand.nextInt(4);
        i[2] = rand.nextInt(4);
        i[3] = rand.nextInt(4);

        Boolean gotOrder = false;

        while(!gotOrder) {
            gotOrder = true;
            if(i[0] == i[1] || i[0] == i[2] || i[0] == i[3]) {
                i[0] = rand.nextInt(4);
                gotOrder = false;
            }
            if(i[1] == i[0] || i[1] == i[2] || i[1] == i[3]) {
                i[1] = rand.nextInt(4);
                gotOrder = false;
            }
            if(i[2] == i[0] || i[2] == i[1] || i[2] == i[3]) {
                i[2] = rand.nextInt(4);
                gotOrder = false;
            }
            if(i[3] == i[0] || i[3] == i[1] || i[3] == i[2]) {
                i[3] = rand.nextInt(4);
                gotOrder = false;
            }
        }

        if(i[0] == 0)
        {
            corr = 0;
        }
        else if(i[1] == 0)
        {
            corr = 1;
        }
        else if(i[2] == 0)
        {
            corr = 2;
        }
        else if(i[3] == 0)
        {
            corr = 3;
        }

        String question = "fact_" + arr[0];
        question = question.toLowerCase();

        Resources resString = this.getResources();

        TextView question_facts = (TextView)findViewById(R.id.question_facts);
        question_facts.setText(resString.getIdentifier(question, "string", this.getPackageName()));

        TextView tv1 = (TextView)findViewById(R.id.q1);
        tv1.setText(resString.getIdentifier(arr[i[0]+1], "string", this.getPackageName()));
        TextView tv2 = (TextView)findViewById(R.id.q2);
        tv2.setText(resString.getIdentifier(arr[i[1]+1], "string", this.getPackageName()));
        TextView tv3 = (TextView)findViewById(R.id.q3);
        tv3.setText(resString.getIdentifier(arr[i[2]+1], "string", this.getPackageName()));
        TextView tv4 = (TextView)findViewById(R.id.q4);
        tv4.setText(resString.getIdentifier(arr[i[3]+1], "string", this.getPackageName()));
    }

    public void checkAnswer(View view) {
        if(alr) {
            return;
        }
        alr = true;

        // set correct one
        ColorDrawable[] corrColor = {new ColorDrawable(getResources().getColor(R.color.colorFacts)), new ColorDrawable(getResources().getColor(R.color.colorCorrect))};
        TransitionDrawable corrTrans = new TransitionDrawable(corrColor);
        ColorDrawable[] incorrColor = {new ColorDrawable(getResources().getColor(R.color.colorFacts)), new ColorDrawable(getResources().getColor(R.color.colorIncorrect))};
        TransitionDrawable incorrTrans = new TransitionDrawable(incorrColor);


        Resources res = this.getResources();
        int resID = res.getIdentifier("linearAnswer" + valueOf(corr+1), "id", this.getPackageName());
        LinearLayout corrLL = (LinearLayout) findViewById(resID);
        try{ corrLL.setBackground(corrTrans);}
        catch(Exception e) {
            corrLL.setBackgroundColor(getResources().getColor(R.color.colorCorrect));
        }

        // set variables
        SharedPreferences prefs;
        SharedPreferences.Editor editor;

        prefs = getSharedPreferences("score", MODE_PRIVATE);
        int games = prefs.getInt("facts_totalGames", 0);
        games++;

        prefs = getSharedPreferences("score", MODE_PRIVATE);
        editor = prefs.edit();
        editor.putInt("facts_totalGames", games);
        editor.commit();


        // do the checking
        int corrID = res.getIdentifier("answer" + valueOf(corr+1), "id", this.getPackageName());

        if(view.getId() == corrID)
        {
            corrTrans.startTransition(50);

            Toast pointToast = Toast.makeText(this, "+3P", Toast.LENGTH_SHORT);
            LinearLayout toastLayout = (LinearLayout) pointToast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(17);
            toastTV.setTypeface(null, Typeface.BOLD);
            pointToast.setGravity(Gravity.TOP|Gravity.RIGHT,15,15);
            pointToast.show();

            // add score
            prefs = getSharedPreferences("score", MODE_PRIVATE);
            int points = prefs.getInt("facts_score", 0);
            points = points + 3;

            prefs = getSharedPreferences("score", MODE_PRIVATE);
            editor = prefs.edit();
            editor.putInt("facts_score", points);
            editor.commit();

            MenuItem settingsMenuItem = mainMenu.findItem(R.id.game_score);
            settingsMenuItem.setTitle(valueOf(points) + " P");
        }
        else if(view.getId() == R.id.answer1){
            LinearLayout wrongLL = (LinearLayout)findViewById(R.id.linearAnswer1);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }
        else if(view.getId() == R.id.answer2){
            LinearLayout wrongLL = (LinearLayout)findViewById(R.id.linearAnswer2);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }
        else if(view.getId() == R.id.answer3){
            LinearLayout wrongLL = (LinearLayout)findViewById(R.id.linearAnswer3);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }
        else if(view.getId() == R.id.answer4){
            LinearLayout wrongLL = (LinearLayout)findViewById(R.id.linearAnswer4);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                LinearLayout reset = (LinearLayout)findViewById(R.id.linearAnswer1);
                reset.setBackgroundColor(getResources().getColor(R.color.colorFacts));
                reset = (LinearLayout)findViewById(R.id.linearAnswer2);
                reset.setBackgroundColor(getResources().getColor(R.color.colorFacts));
                reset = (LinearLayout)findViewById(R.id.linearAnswer3);
                reset.setBackgroundColor(getResources().getColor(R.color.colorFacts));
                reset = (LinearLayout)findViewById(R.id.linearAnswer4);
                reset.setBackgroundColor(getResources().getColor(R.color.colorFacts));
                alr = false;
                readTask();
            }
        }, 1200);
    }

    public void setOnClicks()
    {
        View allViews = findViewById(R.id.answer1);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        allViews = findViewById(R.id.answer2);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        allViews = findViewById(R.id.answer3);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        allViews = findViewById(R.id.answer4);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
    }

}
