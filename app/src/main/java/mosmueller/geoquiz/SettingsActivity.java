package mosmueller.geoquiz;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.os.ConfigurationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TimePicker;

import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {

    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Button timeButton;
    Switch notificationSwitch;
    Button languageButton;
    AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_Settings));

        timeButton = findViewById(R.id.notification_timepicker_button);
        notificationSwitch = findViewById(R.id.notification_switch);

        languageButton = findViewById(R.id.language_button);

        timeButton();
        setTime(readHour(),readMinute());
        switcher();
        setSwitchState();
        languageButton();
        languageButtonSet(readLanguage());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


// Time -----

    private void timeButton() {

        timeButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                TimePickerDialog mTimePicker = new TimePickerDialog(SettingsActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute)
                    {

                        writeTime(selectedHour, selectedMinute);
                        setTime(selectedHour, selectedMinute);

                    }
                }, readHour(), readMinute(), true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

                mTimePicker.getButton(TimePickerDialog.BUTTON_POSITIVE).setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                mTimePicker.getButton(TimePickerDialog.BUTTON_NEGATIVE).setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            }
        });
    }

    private void writeTime(int selectedHour, int selectedMinute)
    {
        // WRITE TO SHARED PREFERENCES
        prefs = getSharedPreferences("score", MODE_PRIVATE);
        editor = prefs.edit();
        editor.putInt("daily_notification_hour", selectedHour);
        editor.putInt("daily_notification_minute", selectedMinute);
        editor.commit();
    }

    private void setTime(int selectedHour, int selectedMinute)
    {
        // SET THE BUTTON TEXT
        String selectedHourButton = String.valueOf(selectedHour);
        String selectedMinuteButton = String.valueOf(selectedMinute);

        if(String.valueOf(selectedHour).length() < 2)
        {
            selectedHourButton = "0" + selectedHour;
        }
        if (String.valueOf(selectedMinute).length() < 2)
        {
            selectedMinuteButton = "0" + selectedMinute;
        }

        timeButton.setText(selectedHourButton + ":" + selectedMinuteButton);
    }

    private int readHour()
    {
        prefs = getSharedPreferences("score", MODE_PRIVATE);
        return prefs.getInt("daily_notification_hour", 0);
    }

    private int readMinute()
    {
        prefs = getSharedPreferences("score", MODE_PRIVATE);
        return prefs.getInt("daily_notification_minute", 0);
    }



// SWITCH -----

    private void switcher()
    {
        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                changeSwitcherText(isChecked);
                writeSwitchState(isChecked);
            }
        });
    }

    private void writeSwitchState(boolean bool)
    {

        // WRITE TO SHARED PREFERENCES
        prefs = getSharedPreferences("score", MODE_PRIVATE);
        editor = prefs.edit();
        editor.putBoolean("settings_notification_set", bool);
        editor.commit();
    }

    private void setSwitchState()
    {
        prefs = getSharedPreferences("score", MODE_PRIVATE);

        if(prefs.getBoolean("settings_notification_set", true))
        {
            notificationSwitch.setChecked(true);
        }
    }

    private void changeSwitcherText(Boolean isChecked)
    {
        if(isChecked)
        {
            notificationSwitch.setText(this.getResources().getIdentifier("settings_notification_switch_on", "string", this.getPackageName()));
        }
        else
        {
            notificationSwitch.setText(this.getResources().getIdentifier("settings_notification_switch_off", "string", this.getPackageName()));
        }

    }

// LANGUAGE -----

    private void languageButton()
    {
        languageButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                builder = new AlertDialog.Builder(SettingsActivity.this);

                // Show dialog if API version is too low
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
                {
                    builder.setTitle(R.string.language_dialog_not_supported_title)
                            .setMessage(R.string.language_dialog_not_supported_message)
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // doing nothing
                                }
                            })
                            .setCancelable(true)
                            .show();
                    return;
                }

                builder.setTitle(SettingsActivity.this.getResources().getIdentifier("language_dialog_title", "string", SettingsActivity.this.getPackageName()));

                // add radio button list
                String[] languages =
                {
                        "" + SettingsActivity.this.getResources().getString(SettingsActivity.this.getResources().getIdentifier("language_en", "string", SettingsActivity.this.getPackageName())),
                        "" + SettingsActivity.this.getResources().getString(SettingsActivity.this.getResources().getIdentifier("language_de", "string", SettingsActivity.this.getPackageName())),
                };

                int checkedItem;
                switch (readLanguage())
                {
                    case "en":
                        checkedItem = 0;
                        break;
                    case "de":
                        checkedItem = 1;
                        break;
                    default:
                        checkedItem = 0;
                        break;
                }

                builder.setSingleChoiceItems(languages, checkedItem, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        // user checked an item
                        ListView lv = ((AlertDialog)dialog).getListView();
                        lv.setTag(new Integer(which));
                    }
                });

                // add OK and Cancel buttons
                builder.setPositiveButton(SettingsActivity.this.getResources().getIdentifier("language_dialog_ok", "string", SettingsActivity.this.getPackageName()), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        ListView lv = ((AlertDialog)dialog).getListView();
                        String selected;

                        if(lv.getTag() == null)
                        {
                            return;
                        }

                        switch ((Integer)lv.getTag())
                        {
                            case 0:
                                selected = "en";
                                break;
                            case 1:
                                selected = "de";
                                break;
                            default:
                                selected = "en";
                                break;
                        }

                        prefs = getSharedPreferences("score", MODE_PRIVATE);
                        editor = prefs.edit();
                        editor.putString("language_set", selected);
                        editor.commit();
                        loadLang();
                        languageButtonSet(selected);
                    }
                });
                builder.setNegativeButton(SettingsActivity.this.getResources().getIdentifier("language_dialog_cancel", "string", SettingsActivity.this.getPackageName()), null);

                AlertDialog dialog = builder.create();
                dialog.show();

                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTypeface(Typeface.DEFAULT, Typeface.BOLD);
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTypeface(Typeface.DEFAULT, Typeface.BOLD);

            }
        });
    }

    private String readLanguage()
    {
        prefs = getSharedPreferences("score", MODE_PRIVATE);

        return prefs.getString("language_set", Locale.getDefault().getLanguage());
    }

    private void languageButtonSet(String lang)
    {
        switch (lang)
        {
            case "en":
                languageButton.setText(this.getResources().getIdentifier("language_en", "string", this.getPackageName()));
                break;
            case "de":
                languageButton.setText(this.getResources().getIdentifier("language_de", "string", this.getPackageName()));
                break;
            default:
                languageButton.setText(this.getResources().getIdentifier("language_en", "string", this.getPackageName()));
                break;
        }
    }

    private void loadLang()
    {
        String locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
        {
            locale = ConfigurationCompat.getLocales(getResources().getConfiguration()).get(0).getLanguage();
        }
        else
        {
            locale = String.valueOf(Locale.getDefault().getLanguage());
        }

        if (!readLanguage().equals(locale))
        {
            //Change Application level locale
            LocaleHelper.setLocale(this, readLanguage());
            //It is required to recreate the activity to reflect the change in UI.
            recreate();
        }
    }
}
