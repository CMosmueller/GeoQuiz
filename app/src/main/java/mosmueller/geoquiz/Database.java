package mosmueller.geoquiz;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

 public class Database {

     public static final String DB_NAME = "geoquiz.db";
     public static final int DB_VERSION = 3;
     int oldVersion = 0;

     public static final String TABLE_COUNTRIES = "countries";
     public static final String COLUMN_CID = "_id";
     public static final String COLUMN_CCOUNTRY = "ccountry";

     public static final String TABLE_FACTS = "facts";
     public static final String COLUMN_QID = "_id";
     public static final String COLUMN_Q1 = "q1";
     public static final String COLUMN_Q2 = "q2";
     public static final String COLUMN_Q3 = "q3";
     public static final String COLUMN_Q4 = "q4";

     public static final String TABLE_US_STATES = "us_states";
     public static final String COLUMN_SID = "_id";
     public static final String COLUMN_STATE = "state";
     public static final String COLUMN_SBORDER = "sborder";

     public static final String TABLE_US_CAPITALS = "us_capitals";
     public static final String COLUMN_US_CID = "_id";
     public static final String COLUMN_CITY = "city";
     public static final String COLUMN_CAPITAL_OF = "capital_of";

     public static final String TABLE_US_STATES_NICKNAMES = "us_states_nicknames";
     public static final String COLUMN_US_NID = "_id";
     public static final String COLUMN_NICKNAME = "nickname";
     public static final String COLUMN_NICKNAME_STATE = "state";

     private Context context;


     public Database(Context context) {
         this.context = context;
     }

     public SQLiteDatabase openDatabase() {
         File dbFile = context.getDatabasePath(DB_NAME);

         SharedPreferences prefs = context.getSharedPreferences("score", Context.MODE_PRIVATE);
         oldVersion = prefs.getInt("dbver", 0);

         if (!dbFile.exists() || oldVersion < DB_VERSION) {
             try {
                 copyDatabase(dbFile);
                 SharedPreferences.Editor editor = prefs.edit();
                 editor.putInt("dbver", DB_VERSION);
                 editor.commit();
             } catch (IOException e) {
                 throw new RuntimeException("Error creating source database", e);
             }
         }

         return SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);
     }

     private void copyDatabase(File dbFile) throws IOException {
         InputStream is = context.getAssets().open(DB_NAME);
         OutputStream os = new FileOutputStream(dbFile);

         byte[] buffer = new byte[1024];
         while (is.read(buffer) > 0) {
             os.write(buffer);
         }

         os.flush();
         os.close();
         is.close();
     }


     public Cursor readC(SQLiteDatabase database) {

         // randomize ID
         Random rand = new Random();
         int i = rand.nextInt(207)+1;

         // Get random tupel
         Cursor cursor = database.query(TABLE_COUNTRIES, new String[]{COLUMN_CID, COLUMN_CCOUNTRY}, COLUMN_CID + " = ?", new String[]{String.valueOf(i)}, null, null, null);

         if(cursor != null && cursor.getCount() > 0) {
             return cursor;
         }
         else {
             return null;
         }
     }


     public Cursor readQ(SQLiteDatabase database) {
         /* Count the tupels */
         /*Cursor cursorMax = database.query(TABLE_FACTS, new String[]{COLUMN_QID}, COLUMN_QID + " =( SELECT max(" + COLUMN_QID + ") FROM " + TABLE_FACTS + ")", null, null, null, null);
         int max = Integer.parseInt(cursorMax.getString(cursorMax.getColumnIndex("_id")));
         cursorMax.close();*/

         // randomize ID
         Random rand = new Random();
         int i = rand.nextInt(113)+1;

         // Get random tupel
         Cursor cursor = database.query(TABLE_FACTS, new String[]{COLUMN_QID, COLUMN_Q1, COLUMN_Q2, COLUMN_Q3, COLUMN_Q4}, COLUMN_QID + " = ?", new String[]{String.valueOf(i)}, null, null, null);

         if(cursor != null && cursor.getCount() > 0) {
             return cursor;
         }
         else {
             return null;
         }
     }


     public Cursor readS(SQLiteDatabase database) {

         // randomize ID
         Random rand = new Random();
         int i = rand.nextInt(50)+1;

         // Get random tupel
         Cursor cursor = database.query(TABLE_US_STATES, new String[]{COLUMN_SID, COLUMN_STATE, COLUMN_SBORDER}, COLUMN_SID + " = ?", new String[]{String.valueOf(i)}, null, null, null);

         if(cursor != null && cursor.getCount() > 0) {
             return cursor;
         }
         else {
             return null;
         }
     }


     public Cursor read_capital(SQLiteDatabase database) {

         // randomize ID
         Random rand = new Random();
         int i = rand.nextInt(50)+1;

         // Get random tupel
         Cursor cursor = database.query(TABLE_US_CAPITALS, new String[]{COLUMN_US_CID, COLUMN_CITY, COLUMN_CAPITAL_OF}, COLUMN_SID + " = ?", new String[]{String.valueOf(i)}, null, null, null);

         if(cursor != null && cursor.getCount() > 0) {
             return cursor;
         }
         else {
             return null;
         }
     }


     public Cursor read_city(SQLiteDatabase database) {

         // randomize ID
         Random rand = new Random();
         int i = rand.nextInt(132)+1;

         // Get random tupel
         Cursor cursor = database.query(TABLE_US_CAPITALS, new String[]{COLUMN_US_CID, COLUMN_CITY, COLUMN_CAPITAL_OF}, COLUMN_SID + " = ?", new String[]{String.valueOf(i)}, null, null, null);

         if(cursor != null && cursor.getCount() > 0) {
             return cursor;
         }
         else {
             return null;
         }
     }


     public Cursor read_nickname(SQLiteDatabase database) {

         // randomize ID
         Random rand = new Random();
         int i = rand.nextInt(210)+1;

         // Get random tupel
         Cursor cursor = database.query(TABLE_US_STATES_NICKNAMES, new String[]{COLUMN_US_NID, COLUMN_NICKNAME, COLUMN_NICKNAME_STATE}, COLUMN_US_NID + " = ?", new String[]{String.valueOf(i)}, null, null, null);

         if(cursor != null && cursor.getCount() > 0) {
             return cursor;
         }
         else {
             return null;
         }
     }

 }
