package mosmueller.geoquiz;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.os.ConfigurationCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));

        setOnClicks();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        drawer.addDrawerListener(new DrawerLayout.DrawerListener(){

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                //Called when a drawer's position changes.
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Called when a drawer has settled in a completely open state.
                // The drawer is interactive at this point.
                // If you have 2 drawers (left and right) you can distinguish
                // them by using id of the drawerView. int id = drawerView.getId();
                // id will be your layout's id: for example R.id.left_drawer
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                // Called when a drawer has settled in a completely closed state.
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                // Called when the drawer motion state changes. The new state will be one of STATE_IDLE, STATE_DRAGGING or STATE_SETTLING.
                try{
                    setDrawerStats();
                } catch(Exception e){}
            }
        });

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Refresh stats in case of rotation
        setDrawerStats();

        // show SVG (Flag) - prevent displaying issues with XML-Icon
        ImageView imageView;

        imageView = (ImageView) findViewById(R.id.ic_flags);
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        try {
            SVG svg = SVG.getFromResource(this, R.raw.icon_flag);
            Drawable drawable = new PictureDrawable(svg.renderToPicture());
            imageView.setImageDrawable(drawable);
        }
        catch(SVGParseException e){}


        // Show Rate-Me
        SharedPreferences prefs = getSharedPreferences("score", MODE_PRIVATE);
        int overall = prefs.getInt("level", 0);
        int asked = prefs.getInt("rate_me_asked", 0);

        if(asked == 0 && overall >= 2)
        {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("rate_me_asked", 1);
            editor.commit();

            AlertDialog.Builder builder;

            builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.rate_me_title)
                    .setMessage(R.string.rate_me_message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // doing nothing
                        }
                    })
                    .setCancelable(true)
                    .show();
        }

// NOTIFICATION -----
        // region notification

        //PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        //SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        prefs = getSharedPreferences("score", MODE_PRIVATE);
        Boolean dailyNotify =  prefs.getBoolean("settings_notification_set", true);

        PackageManager pm = this.getPackageManager();
        ComponentName receiver = new ComponentName(this, DeviceBootReceiver.class);
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

// if user enabled daily notifications
        if (dailyNotify)
        {

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, prefs.getInt("daily_notification_hour", 0));
            calendar.set(Calendar.MINUTE, prefs.getInt("daily_notification_minute", 0));
            calendar.set(Calendar.SECOND, 0);

            // if notification time is before selected time or dailies are already done, send notification the next day
            int done = prefs.getInt("daily_done", 0);
            String dailyDate = prefs.getString("daily_date", "");
            Calendar dailyCalendar = Calendar.getInstance();
            SimpleDateFormat today = new SimpleDateFormat("yyyy / MM / dd ");
            String strDate = today.format(dailyCalendar.getTime());

            if (calendar.before(Calendar.getInstance()) || (done == 10 && dailyDate.equals(strDate)) )
            {
                calendar.add(Calendar.DATE, 1);
            }
            if (manager != null)
            {
                manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                        AlarmManager.INTERVAL_DAY, pendingIntent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                {
                    manager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                }
            }
            // To enable Boot Receiver class
            pm.setComponentEnabledSetting(receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
        }

        // Disable Daily Notifications
        else
        {
            if (PendingIntent.getBroadcast(this, 0, alarmIntent, 0) != null && manager != null)
            {
                manager.cancel(pendingIntent);
                // Toast.makeText(this,"Notifications were disabled",Toast.LENGTH_SHORT).show();
            }
            pm.setComponentEnabledSetting(receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
        }
        // endregion notification

    }

    @Override
    protected void attachBaseContext(Context base) {
        // Get SYSTEM-LANGUAGE if the app launches the first time and set it default
        super.attachBaseContext(LocaleHelper.onAttach(base, ConfigurationCompat.getLocales(Resources.getSystem().getConfiguration()).get(0).getLanguage()));
    }

    public void setDrawerStats() {
        SharedPreferences prefs = getSharedPreferences("score", MODE_PRIVATE);
        int level = prefs.getInt("level", 0);
        try {
            TextView levelTV = (TextView)findViewById(R.id.levelTV);
            levelTV.setText("LEVEL " + String.valueOf(level));
        } catch (Exception e){}

        int flagScore = prefs.getInt("flags_score", 0);
        int countryScore = prefs.getInt("countries_score", 0);
        int factScore = prefs.getInt("facts_score", 0);
        int stateScore = prefs.getInt("us_score", 0);
        int stateCapitalScore = prefs.getInt("us_capitals_score", 0);
        int stateNicknameScore = prefs.getInt("us_states_nicknames_score", 0);
        int dailyScore = prefs.getInt("daily_score", 0);
        int overallScore = flagScore + countryScore + factScore + stateScore + stateCapitalScore + stateNicknameScore + dailyScore;

        int origFlagGames = prefs.getInt("flags_totalGames", 0);
        int origCountryGames = prefs.getInt("countries_totalGames", 0);
        int origFactGames = prefs.getInt("facts_totalGames", 0);
        int origStateGames = prefs.getInt("us_totalGames", 0);
        int origStateCapitalGames = prefs.getInt("us_capitals_totalGames", 0);
        int origStateNicknameGames = prefs.getInt("us_states_nicknames_totalGames", 0);
        int origDailyGames = prefs.getInt("daily_totalGames", 0);
        int origOverallGames = origFlagGames + origCountryGames + origFactGames + origStateGames + origStateCapitalGames +origStateNicknameGames + origDailyGames;

        int overallGames = origOverallGames;
        int flagGames = origFlagGames;
        int countryGames = origCountryGames;
        int factGames = origFactGames;
        int stateGames = origStateGames;
        int stateCapitalGames = origStateCapitalGames;
        int stateNicknameGames = origStateNicknameGames;
        int dailyGames = origDailyGames;

        // prevent division by 0
        if(origOverallGames == 0) {overallGames = 1;}
        if(origFlagGames == 0) {flagGames = 1;}
        if(origCountryGames == 0) {countryGames = 1;}
        if(origFactGames == 0) {factGames = 1;}
        if(origStateGames == 0) {stateGames = 1;}
        if(origStateCapitalGames == 0) {stateCapitalGames = 1;}
        if(origStateNicknameGames == 0) {stateNicknameGames = 1;}
        if(origDailyGames == 0) {dailyGames = 1;}

        float flagAcc = (float)flagScore/3f/(float)flagGames*100f;
        float countryAcc = (float)countryScore/3f/(float)countryGames*100f;
        float factAcc = (float)factScore/3f/(float)factGames*100f;
        float stateAcc = (float)stateScore/(float)stateGames*100f;
        float stateCapitalAcc = (float)stateCapitalScore/2f/(float)stateCapitalGames*100f;
        float stateNicknameAcc = (float)stateNicknameScore/3f/(float)stateNicknameGames*100f;
        float dailyAcc = (float)dailyScore/10f/(float)dailyGames*100f;

        float overallAccuracy = (flagAcc/100f*(float)flagGames + countryAcc/100f*(float)countryGames + factAcc/100f*(float)factGames + stateAcc/100f*(float)stateGames + stateCapitalAcc/100f*(float)stateCapitalGames + stateNicknameAcc/100f*(float)stateNicknameGames + dailyAcc/100f*(float)dailyGames)/(float)overallGames*100f;

        MenuItem setItems = navigationView.getMenu().findItem(R.id.overallPoints);
        String s = getString(R.string.points);
        setItems.setTitle(String.valueOf(overallScore) + " " + s);

        setItems = navigationView.getMenu().findItem(R.id.overallAccuracy);
        s = getString(R.string.accuracy);
        setItems.setTitle(String.format("%.2f", overallAccuracy) + "% " + s);


        int missingPoints = 0;

        if(overallScore < 100) {
            missingPoints = 100-overallScore;
        }
        else if(overallScore < 500) {
            missingPoints = 500-overallScore;
        }
        else if(overallScore < 1500) {
            missingPoints = 1500-overallScore;
        }
        else if(overallScore < 3000) {
            missingPoints = 3000-overallScore;
        }
        else if(overallScore < 6000) {
            missingPoints = 6000-overallScore;
        }
        else if(overallScore < 12000) {
            missingPoints = 12000-overallScore;
        }
        else if(overallScore < 25000) {
            missingPoints = 25000-overallScore;
        }
        else if(overallScore < 50000) {
            missingPoints = 50000-overallScore;
        }
        else if(overallScore < 75000) {
            missingPoints = 75000-overallScore;
        }
        else if(overallScore < 100000) {
            missingPoints = 100000-overallScore;
        }

        if(level == 10)
        {
            setItems = navigationView.getMenu().findItem(R.id.nextLevel);
            s = getString(R.string.max_level);
            setItems.setTitle(s);
        }
        else {
            setItems = navigationView.getMenu().findItem(R.id.nextLevel);
            s = getString(R.string.next_level);
            setItems.setTitle(String.valueOf(missingPoints) + " " + s + " " + String.valueOf(level+1) + "!");
        }

        s = getString(R.string.accuracy);
        setItems = navigationView.getMenu().findItem(R.id.flagAccuracy);
        setItems.setTitle(String.format("%.2f", flagAcc) + "% " + s);

        setItems = navigationView.getMenu().findItem(R.id.countryAccuracy);
        setItems.setTitle(String.format("%.2f", countryAcc) + "% " + s);

        setItems = navigationView.getMenu().findItem(R.id.factAccuracy);
        setItems.setTitle(String.format("%.2f", factAcc) + "% " + s);

        setItems = navigationView.getMenu().findItem(R.id.stateAccuracy);
        setItems.setTitle(String.format("%.2f", stateAcc) + "% " + s);

        setItems = navigationView.getMenu().findItem(R.id.stateCapitalAccuracy);
        setItems.setTitle(String.format("%.2f", stateCapitalAcc) + "% " + s);

        setItems = navigationView.getMenu().findItem(R.id.stateNicknameAccuracy);
        setItems.setTitle(String.format("%.2f", stateNicknameAcc) + "% " + s);

        setItems = navigationView.getMenu().findItem(R.id.dailyAccuracy);
        setItems.setTitle(String.format("%.2f", dailyAcc) + "% " + s);


        s = getString(R.string.games);
        setItems = navigationView.getMenu().findItem(R.id.flagGames);
        setItems.setTitle(s + ": " + String.valueOf(origFlagGames));

        setItems = navigationView.getMenu().findItem(R.id.countryGames);
        setItems.setTitle(s + ": " + String.valueOf(origCountryGames));

        setItems = navigationView.getMenu().findItem(R.id.factGames);
        setItems.setTitle(s + ": " + String.valueOf(origFactGames));

        setItems = navigationView.getMenu().findItem(R.id.stateGames);
        setItems.setTitle(s + ": " + String.valueOf(origStateGames));

        setItems = navigationView.getMenu().findItem(R.id.stateCapitalGames);
        setItems.setTitle(s + ": " + String.valueOf(origStateCapitalGames));

        setItems = navigationView.getMenu().findItem(R.id.stateNicknameGames);
        setItems.setTitle(s + ": " + String.valueOf(origStateNicknameGames));

        setItems = navigationView.getMenu().findItem(R.id.dailyGames);
        setItems.setTitle(s + ": " + String.valueOf(origDailyGames));
    }


    @Override
    public void onResume(){
        super.onResume();

        // Check Level
        SharedPreferences prefs = getSharedPreferences("score", MODE_PRIVATE);
        int overallScore = prefs.getInt("flags_score", 0);
        overallScore = overallScore + prefs.getInt("countries_score", 0);
        overallScore = overallScore + prefs.getInt("facts_score", 0);
        overallScore = overallScore + prefs.getInt("us_score", 0);
        overallScore = overallScore + prefs.getInt("us_capitals_score", 0);
        overallScore = overallScore + prefs.getInt("us_states_nicknames_score", 0);
        overallScore = overallScore + prefs.getInt("daily_score", 0);

        int level = prefs.getInt("level", 0);
        String levelUpStr = getString(R.string.level_up);

        if(overallScore >= 100000) {

            if(level != 10) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 10!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 10);
                editor.commit();
            }
        }
        else if(overallScore >= 75000) {

            if(level != 9) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 9!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 9);
                editor.commit();
            }
        }
        else if(overallScore >= 50000) {

            if(level != 8) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 8!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 8);
                editor.commit();
            }
        }
        else if(overallScore >= 25000) {

            if(level != 7) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 7!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 7);
                editor.commit();
            }
        }
        else if(overallScore >= 12000) {

            if(level != 6) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 6!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 6);
                editor.commit();
            }
        }
        else if(overallScore >= 6000) {

            if(level != 5) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 5!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 5);
                editor.commit();
            }
        }
        else if(overallScore >= 3000) {

            if(level != 4) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 4!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 4);
                editor.commit();
            }
        }
        else if(overallScore >= 1500) {

            if(level != 3) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 3!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 3);
                editor.commit();
            }
        }
        else if(overallScore >= 500) {

            if(level != 2) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 2!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 2);
                editor.commit();
            }
        }
        else if(overallScore >= 100) {

            if(level != 1) {
                Toast levelUp = Toast.makeText(this, levelUpStr + " 1!", Toast.LENGTH_LONG);
                levelUp.show();
                prefs = getSharedPreferences("score", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("level", 1);
                editor.commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.license_menu) {

            // Dialog:
            AlertDialog.Builder builder;

            builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.license_menu_title)
                    .setMessage(R.string.license_menu_content)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // doing nothing
                        }
                    })
                    .setCancelable(true)
                    .show();
            return true;
        }
        else if (id == R.id.settings_menu)
        {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        /*int id = item.getItemId();

        if (id == R.id.nav_share) {}
        else if (id == R.id.nav_send) {}

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);*/
        return true;
    }


    public void share_btn_clicked (View view)
    {
        SharedPreferences prefs = getSharedPreferences("score", MODE_PRIVATE);
        int level = prefs.getInt("level", 0);

        int overallScore = prefs.getInt("flags_score", 0);
        overallScore = overallScore + prefs.getInt("countries_score", 0);
        overallScore = overallScore + prefs.getInt("facts_score", 0);
        overallScore = overallScore + prefs.getInt("us_score", 0);
        overallScore = overallScore + prefs.getInt("us_capitals_score", 0);
        overallScore = overallScore + prefs.getInt("us_states_nicknames_score", 0);
        overallScore = overallScore + prefs.getInt("daily_score", 0);


        String s1 = getString(R.string.message_text);
        String s2 = getString(R.string.message_text2);
        String s3 = getString(R.string.message_text3);
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, s1 + " " + String.valueOf(level) + " " + s2 + " " + String.valueOf(overallScore) + " " + s3);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_chooser_title)));
    }


    public void createNewActivity (View view)
    {
        Intent intent;

        switch (view.getId()){
            case R.id.flags:
                intent = new Intent(this, FlagsActivity.class);
                startActivity(intent);
                break;
            case R.id.countries:
                intent = new Intent(this, CountriesActivity.class);
                startActivity(intent);
                break;
            case R.id.facts:
                intent = new Intent(this, FactsActivity.class);
                startActivity(intent);
                break;
            case R.id.us_states:
                intent = new Intent(this, US_statesActivity.class);
                startActivity(intent);
                break;
            case R.id.us_capitals:
                intent = new Intent(this, US_capitalsActivity.class);
                startActivity(intent);
                break;
            case R.id.us_states_nicknames:
                intent = new Intent(this, US_states_nicknamesActivity.class);
                startActivity(intent);
                break;
            case R.id.daily:

                SharedPreferences prefs = getSharedPreferences("score", MODE_PRIVATE);
                int done = prefs.getInt("daily_done", 0);

                prefs = getSharedPreferences("score", MODE_PRIVATE);
                String dailyDate = prefs.getString("daily_date", "");

                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat today = new SimpleDateFormat("yyyy / MM / dd ");
                String strDate = today.format(calendar.getTime());

                if(done < 10 || !dailyDate.equals(strDate)) {

                    if (!dailyDate.equals(strDate)) {
                        prefs = getSharedPreferences("score", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putInt("daily_done", 0);
                        editor.putString("daily_date", strDate);
                        editor.commit();
                    }
                    intent = new Intent(this, DailyActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast dailyToast = Toast.makeText(this, R.string.daily_toast, Toast.LENGTH_SHORT);
                    dailyToast.show();
                }

                break;
        }

    }

    public void setOnClicks()
    {
        View allViews = findViewById(R.id.flags);
        allViews.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                createNewActivity(v);

            }
        });
        allViews = findViewById(R.id.countries);
        allViews.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                createNewActivity(v);
            }
        });
        allViews = findViewById(R.id.facts);
        allViews.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                createNewActivity(v);
            }
        });
        allViews = findViewById(R.id.us_states);
        allViews.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                createNewActivity(v);
            }
        });
        allViews = findViewById(R.id.us_capitals);
        allViews.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                createNewActivity(v);
            }
        });
        allViews = findViewById(R.id.us_states_nicknames);
        allViews.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                createNewActivity(v);
            }
        });
        allViews = findViewById(R.id.daily);
        allViews.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {

                createNewActivity(v);
            }
        });
    }

}
