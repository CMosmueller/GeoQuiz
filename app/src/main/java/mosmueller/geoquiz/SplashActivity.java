package mosmueller.geoquiz;

import android.content.Intent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


/**
 * Created by Mosmüller on 15.12.2017.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}