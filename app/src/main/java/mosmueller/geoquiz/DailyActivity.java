package mosmueller.geoquiz;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

import static java.lang.String.valueOf;

public class DailyActivity extends AppCompatActivity {

    Random rand = new Random();
    int iDaily;
    Menu mainMenu;
    boolean alr = false;
    int iCorr;
    String arr[][] = new String[5][3];
    String swap_helper[][] = new String[1][3];
    
    String prevFlag;
    String prevCountry;
    String prevFact;
    String prevState;
    String prevCapital;
    String prevStateNickname;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily);
        getSupportActionBar().setTitle(getResources().getString(R.string.title_DailyActivity));

        iDaily = rand.nextInt(6);
        setOnClicks();
        readTask();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.game_menu, menu);
        mainMenu = menu;

        SharedPreferences prefs = getSharedPreferences("score", MODE_PRIVATE);
        int done = prefs.getInt("daily_done", 0);
        MenuItem settingsMenuItem = mainMenu.findItem(R.id.game_score);
        settingsMenuItem.setTitle(valueOf(done) + "/10");

        return true;
    }


    public void readTask() {

        SharedPreferences prefs = getSharedPreferences("score", MODE_PRIVATE);
        String dailyDate = prefs.getString("daily_date", null);

        prefs = getSharedPreferences("score", MODE_PRIVATE);
        int done = prefs.getInt("daily_done", 0);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat today = new SimpleDateFormat("yyyy / MM / dd ");
        String strDate = today.format(calendar.getTime());

        if (!dailyDate.equals(strDate)) {
            prefs = getSharedPreferences("score", MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("daily_done", 0);
            editor.putString("daily_date", strDate);
            editor.commit();
            MenuItem settingsMenuItem = mainMenu.findItem(R.id.game_score);
            settingsMenuItem.setTitle(valueOf(0) + "/10");
        }
        else if(done == 10) {
            Toast dailyToast = Toast.makeText(this, R.string.daily_toast, Toast.LENGTH_SHORT);
            dailyToast.show();

            onBackPressed();
        }


        Database db = new Database(this);
        SQLiteDatabase sqldb = db.openDatabase();

        if(iDaily == 0) {

            // Reset array
            arr[0][0] = null;
            arr[1][0] = null;
            arr[2][0] = null;
            arr[3][0] = null;

            Cursor c;
            for(int four = 0; four<4; four++)
            {
                c = db.readC(sqldb);

                if(c != null && c.getCount() > 0) {
                    c.moveToFirst();
                    String checkID = c.getString(0);

                    if(arr[0][0]==null)
                    {
                        arr[0][0]=c.getString(0);
                        arr[0][1]=c.getString(1);
                    }
                    else if(arr[1][0]==null && !arr[0][0].equals(checkID))
                    {
                        arr[1][0]=c.getString(0);
                        arr[1][1]=c.getString(1);
                    }
                    else if(arr[2][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID))
                    {
                        arr[2][0]=c.getString(0);
                        arr[2][1]=c.getString(1);
                    }
                    else if(arr[3][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID) && !arr[2][0].equals(checkID))
                    {
                        arr[3][0]=c.getString(0);
                        arr[3][1]=c.getString(1);
                    }
                    else{
                        four--;
                    }
                }
                else { // ERROR!
                    Log.d("DB","Kein Eintrag vorhanden!");
                }

                if(four==3)
                {
                    c.close();
                }
            }
        }

        else if(iDaily == 1) {

            // Reset array
            arr[0][0] = null;
            arr[1][0] = null;
            arr[2][0] = null;
            arr[3][0] = null;

            Cursor c;
            for(int four = 0; four<4; four++)
            {
                c = db.readC(sqldb);

                if(c != null && c.getCount() > 0) {
                    c.moveToFirst();
                    String checkID = c.getString(0);

                    if(arr[0][0]==null)
                    {
                        arr[0][0]=c.getString(0);
                        arr[0][1]=c.getString(1);
                    }
                    else if(arr[1][0]==null && !arr[0][0].equals(checkID))
                    {
                        arr[1][0]=c.getString(0);
                        arr[1][1]=c.getString(1);
                    }
                    else if(arr[2][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID))
                    {
                        arr[2][0]=c.getString(0);
                        arr[2][1]=c.getString(1);
                    }
                    else if(arr[3][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID) && !arr[2][0].equals(checkID))
                    {
                        arr[3][0]=c.getString(0);
                        arr[3][1]=c.getString(1);
                    }
                    else{
                        four--;
                    }
                }
                else { // ERROR!
                    Log.d("DB","Kein Eintrag vorhanden!");
                }

                if(four==3)
                {
                    c.close();
                }
            }
        }

        else if(iDaily == 2) {

            Cursor c = db.readQ(sqldb);
            if(c != null && c.getCount() > 0) {
                c.moveToFirst();

                Boolean gotFact = false;
                while(!gotFact)
                {
                    if(!c.getString(0).equals(prevFact))
                    {
                        arr[0][0]=c.getString(0);
                        arr[1][0]=c.getString(1);
                        arr[2][0]=c.getString(2);
                        arr[3][0]=c.getString(3);
                        arr[4][0]=c.getString(4);

                        prevFact = c.getString(0);
                        gotFact = true;
                    }
                    else {
                        c = db.readQ(sqldb);
                        c.moveToFirst();
                    }
                }
            }
            else { // ERROR!
                Log.d("DB","Kein Eintrag vorhanden!");
            }
        }

        else if(iDaily == 3) {

            // Reset array
            arr[0][0] = null;
            arr[1][0] = null;
            arr[2][0] = null;
            arr[3][0] = null;

            Cursor c;
            for(int four = 0; four<4; four++)
            {
                c = db.readS(sqldb);

                if(c != null && c.getCount() > 0) {
                    c.moveToFirst();
                    String checkID = c.getString(0);

                    if(arr[0][0]==null)
                    {
                        arr[0][0]=c.getString(0);
                        arr[0][1]=c.getString(1);
                        arr[0][2]=c.getString(2);
                    }
                    else if(arr[1][0]==null && !arr[0][0].equals(checkID))
                    {
                        arr[1][0]=c.getString(0);
                        arr[1][1]=c.getString(1);
                        arr[1][2]=c.getString(2);
                    }
                    else if(arr[2][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID))
                    {
                        arr[2][0]=c.getString(0);
                        arr[2][1]=c.getString(1);
                        arr[2][2]=c.getString(2);
                    }
                    else if(arr[3][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID) && !arr[2][0].equals(checkID))
                    {
                        arr[3][0]=c.getString(0);
                        arr[3][1]=c.getString(1);
                        arr[3][2]=c.getString(2);
                    }
                    else{
                        four--;
                    }
                }
                else { // ERROR!
                    Log.d("DB","Kein Eintrag vorhanden!");
                }

                if(four==3)
                {
                    c.close();
                }
            }
        }

        else if(iDaily == 4) {

            // Reset array
            arr[0][0] = null;
            arr[1][0] = null;
            arr[2][0] = null;
            arr[3][0] = null;

            Cursor c;
            for(int four = 0; four<4; four++)
            {
                if(four == 0)
                {
                    c = db.read_capital(sqldb);
                }
                else
                {
                    c = db.read_city(sqldb);
                }

                if(c != null && c.getCount() > 0) {
                    c.moveToFirst();
                    //Log.d("DB","Eintrag vorhanden: " + c.getString(0) + " " + c.getString(1) + " " + c.getString(2));

                    String checkID = c.getString(0);

                    if(arr[0][0]==null)
                    {
                        arr[0][0]=c.getString(0);
                        arr[0][1]=c.getString(1);
                        arr[0][2]=c.getString(2);

                        if(prevCapital != null && prevCapital.equals(arr[0][0])) {
                            arr[0][0] = null;
                            four--;
                        }
                        else
                        {
                            prevCapital = arr[0][0];
                        }
                    }
                    else if(arr[1][0]==null && !arr[0][0].equals(checkID))
                    {
                        arr[1][0]=c.getString(0);
                        arr[1][1]=c.getString(1);
                        arr[1][2]=c.getString(2);
                    }
                    else if(arr[2][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID))
                    {
                        arr[2][0]=c.getString(0);
                        arr[2][1]=c.getString(1);
                        arr[2][2]=c.getString(2);
                    }
                    else if(arr[3][0]==null && !arr[0][0].equals(checkID) && !arr[1][0].equals(checkID) && !arr[2][0].equals(checkID))
                    {
                        arr[3][0]=c.getString(0);
                        arr[3][1]=c.getString(1);
                        arr[3][2]=c.getString(2);
                    }
                    else{
                        four--;
                    }
                }
                else { // ERROR!
                    Log.d("DB","Kein Eintrag vorhanden!");
                }

                if(four==3)
                {
                    c.close();
                }
            }
        }

        else if(iDaily == 5) {

            // Reset array
            arr[0][0] = null;
            arr[1][0] = null;
            arr[2][0] = null;
            arr[3][0] = null;

            Cursor c;
            for(int four = 0; four<4; four++)
            {
                c = db.read_nickname(sqldb);

                if(c != null && c.getCount() > 0) {
                    c.moveToFirst();
                    //Log.d("DB","Eintrag vorhanden: " + c.getString(0) + " " + c.getString(1) + " " + c.getString(2));

                    String checkState = c.getString(2);

                    if(arr[0][0]==null)
                    {
                        arr[0][0]=c.getString(0);
                        arr[0][1]=c.getString(1);
                        arr[0][2]=c.getString(2);
                    }
                    else if(arr[1][0]==null && !arr[0][2].equals(checkState))
                    {
                        arr[1][0]=c.getString(0);
                        arr[1][1]=c.getString(1);
                        arr[1][2]=c.getString(2);
                    }
                    else if(arr[2][0]==null && !arr[0][2].equals(checkState) && !arr[1][2].equals(checkState))
                    {
                        arr[2][0]=c.getString(0);
                        arr[2][1]=c.getString(1);
                        arr[2][2]=c.getString(2);
                    }
                    else if(arr[3][0]==null && !arr[0][2].equals(checkState) && !arr[1][2].equals(checkState) && !arr[2][2].equals(checkState))
                    {
                        arr[3][0]=c.getString(0);
                        arr[3][1]=c.getString(1);
                        arr[3][2]=c.getString(2);
                    }
                    else{
                        four--;
                    }
                }
                else { // ERROR!
                    Log.d("DB","Kein Eintrag vorhanden!");
                }

                if(four==3)
                {
                    c.close();
                }
            }
        }

        sqldb.close();
        showTask();
    }

    public void showTask() {

        // Get TextViews
        TextView question_daily = (TextView)findViewById(R.id.question_daily);
        TextView tv1 = (TextView)findViewById(R.id.q1);
        TextView tv2 = (TextView)findViewById(R.id.q2);
        TextView tv3 = (TextView)findViewById(R.id.q3);
        TextView tv4 = (TextView)findViewById(R.id.q4);

        // Reset font-style (for state-nicknames)
        tv1.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tv2.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tv3.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tv4.setTypeface(Typeface.DEFAULT, Typeface.BOLD);


        if(iDaily == 0) {

            question_daily.setText(R.string.question_flags);

            // Get random correct answer
            iCorr = rand.nextInt(4);

            if(prevFlag != null) {
                while(prevFlag.equals(arr[iCorr][0])){
                    iCorr = rand.nextInt(4);
                }
            }

            prevFlag =  arr[iCorr][0];

            String image = "flag_" + arr[iCorr][1];
            image = image.toLowerCase();

            // show SVG
            ImageView imageView = (ImageView) findViewById(R.id.daily_ImageView);
            imageView.setVisibility(View.VISIBLE);
            imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            try {
                // Build Resource
                Resources res = this.getResources();
                int resID = res.getIdentifier(image, "raw", this.getPackageName());

                // Show
                SVG svg = SVG.getFromResource(this, resID);
                Drawable drawable = new PictureDrawable(svg.renderToPicture());
                imageView.setImageDrawable(drawable);
            }
            catch(SVGParseException e){
                Log.d("DB ", "Error with the following entry: " + arr[iCorr][0]);
            }

            Resources resString = this.getResources();

            tv1.setText(resString.getIdentifier(arr[0][1], "string", this.getPackageName()));
            tv2.setText(resString.getIdentifier(arr[1][1], "string", this.getPackageName()));
            tv3.setText(resString.getIdentifier(arr[2][1], "string", this.getPackageName()));
            tv4.setText(resString.getIdentifier(arr[3][1], "string", this.getPackageName()));
        }

        else if(iDaily == 1) {

            question_daily.setText(R.string.question_countries);

            // Get random correct answer
            iCorr = rand.nextInt(4);

            if(prevCountry != null) {
                while(prevCountry.equals(arr[iCorr][0])){
                    iCorr = rand.nextInt(4);
                }
            }

            prevCountry =  arr[iCorr][0];

            String image = "country_" + arr[iCorr][1];
            image = image.toLowerCase();

            // show SVG
            ImageView imageView = (ImageView) findViewById(R.id.daily_ImageView);
            imageView.setVisibility(View.VISIBLE);
            imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            try {
                // Build Resource
                Resources res = this.getResources();
                int resID = res.getIdentifier(image, "raw", this.getPackageName());

                // Show
                SVG svg = SVG.getFromResource(this, resID);
                Drawable drawable = new PictureDrawable(svg.renderToPicture());
                imageView.setImageDrawable(drawable);
            }
            catch(SVGParseException e){
                Log.d("DB ", "Error with the following entry: " + arr[iCorr][0]);
            }

            Resources resString = this.getResources();

            tv1.setText(resString.getIdentifier(arr[0][1], "string", this.getPackageName()));
            tv2.setText(resString.getIdentifier(arr[1][1], "string", this.getPackageName()));
            tv3.setText(resString.getIdentifier(arr[2][1], "string", this.getPackageName()));
            tv4.setText(resString.getIdentifier(arr[3][1], "string", this.getPackageName()));

        }

        else if(iDaily == 2) {

            // Get random order
            int i[] = new int[4];
            i[0] = rand.nextInt(4);
            i[1] = rand.nextInt(4);
            i[2] = rand.nextInt(4);
            i[3] = rand.nextInt(4);

            Boolean gotOrder = false;

            while(!gotOrder) {
                gotOrder = true;
                if(i[0] == i[1] || i[0] == i[2] || i[0] == i[3]) {
                    i[0] = rand.nextInt(4);
                    gotOrder = false;
                }
                if(i[1] == i[0] || i[1] == i[2] || i[1] == i[3]) {
                    i[1] = rand.nextInt(4);
                    gotOrder = false;
                }
                if(i[2] == i[0] || i[2] == i[1] || i[2] == i[3]) {
                    i[2] = rand.nextInt(4);
                    gotOrder = false;
                }
                if(i[3] == i[0] || i[3] == i[1] || i[3] == i[2]) {
                    i[3] = rand.nextInt(4);
                    gotOrder = false;
                }
            }

            if(i[0] == 0)
            {
                iCorr = 0;
            }
            else if(i[1] == 0)
            {
                iCorr = 1;
            }
            else if(i[2] == 0)
            {
                iCorr = 2;
            }
            else if(i[3] == 0)
            {
                iCorr = 3;
            }


            String question = "fact_" + arr[0][0];
            question = question.toLowerCase();

            Resources resString = this.getResources();

            TextView question_facts = (TextView)findViewById(R.id.question_facts);
            question_facts.setText(resString.getIdentifier(question, "string", this.getPackageName()));

            tv1.setText(resString.getIdentifier(arr[i[0]+1][0], "string", this.getPackageName()));
            tv2.setText(resString.getIdentifier(arr[i[1]+1][0], "string", this.getPackageName()));
            tv3.setText(resString.getIdentifier(arr[i[2]+1][0], "string", this.getPackageName()));
            tv4.setText(resString.getIdentifier(arr[i[3]+1][0], "string", this.getPackageName()));
        }

        else if(iDaily == 3) {

            question_daily.setText(R.string.question_us_states);

            // Get random correct answer
            iCorr = rand.nextInt(4);

            if(prevState != null) {
                while(prevState.equals(arr[iCorr][0])){
                    iCorr = rand.nextInt(4);
                }
            }

            prevState =  arr[iCorr][0];
            String image = "state_" + arr[iCorr][2];
            image = image.toLowerCase();


            // show SVG
            ImageView imageView = (ImageView) findViewById(R.id.daily_ImageView);
            imageView.setVisibility(View.VISIBLE);
            imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            try {
                // Build Resource
                Resources res = this.getResources();
                int resID = res.getIdentifier(image, "raw", this.getPackageName());

                // Show
                SVG svg = SVG.getFromResource(this, resID);
                Drawable drawable = new PictureDrawable(svg.renderToPicture());
                imageView.setImageDrawable(drawable);
            }
            catch(SVGParseException e){}

            tv1.setText(arr[0][1]);
            tv2.setText(arr[1][1]);
            tv3.setText(arr[2][1]);
            tv4.setText(arr[3][1]);
        }

        else if(iDaily == 4) {

            question_daily.setText(R.string.question_us_capitals);

            // Get random correct answer
            iCorr = rand.nextInt(4);

            if(iCorr != 0)
            {
                swap_helper[0][0] = arr[iCorr][0];
                swap_helper[0][1] = arr[iCorr][1];
                swap_helper[0][2] = arr[iCorr][2];

                arr[iCorr][0] = arr[0][0];
                arr[iCorr][1] = arr[0][1];
                arr[iCorr][2] = arr[0][2];

                arr[0][0] = swap_helper[0][0];
                arr[0][1] = swap_helper[0][1];
                arr[0][2] = swap_helper[0][2];
            }

            String image = "state_" + arr[iCorr][2];
            image = image.toLowerCase();


            // show SVG
            ImageView imageView = (ImageView) findViewById(R.id.daily_ImageView);
            imageView.setVisibility(View.VISIBLE);
            imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            try {
                // Build Resource
                Resources res = this.getResources();
                int resID = res.getIdentifier(image, "raw", this.getPackageName());

                // Show
                SVG svg = SVG.getFromResource(this, resID);
                Drawable drawable = new PictureDrawable(svg.renderToPicture());
                imageView.setImageDrawable(drawable);
            }
            catch(SVGParseException e){}

            tv1.setText(arr[0][1]);
            tv2.setText(arr[1][1]);
            tv3.setText(arr[2][1]);
            tv4.setText(arr[3][1]);
        }

        else if(iDaily == 5) {

            question_daily.setText(R.string.question_us_states_nicknames);

            // Get random correct answer
            iCorr = rand.nextInt(4);

            if(prevStateNickname != null) {
                while(prevStateNickname.equals(arr[iCorr][0])){
                    iCorr = rand.nextInt(4);
                }
            }

            prevStateNickname =  arr[iCorr][0];

            String image = "state_" + arr[iCorr][2];
            image = image.toLowerCase();


            // show SVG
            ImageView imageView = (ImageView) findViewById(R.id.daily_ImageView);
            imageView.setVisibility(View.VISIBLE);
            imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            try {
                // Build Resource
                Resources res = this.getResources();
                int resID = res.getIdentifier(image, "raw", this.getPackageName());

                // Show
                SVG svg = SVG.getFromResource(this, resID);
                Drawable drawable = new PictureDrawable(svg.renderToPicture());
                imageView.setImageDrawable(drawable);
            }
            catch(SVGParseException e){}


            // Set other style for official/very popular nicknames
            if(arr[0][1].startsWith("*"))
            {
                arr[0][1] = arr[0][1].substring(1);
                tv1.setTypeface(Typeface.SERIF, Typeface.BOLD_ITALIC);
            }
            if(arr[1][1].startsWith("*"))
            {
                arr[1][1] = arr[1][1].substring(1);
                tv2.setTypeface(Typeface.SERIF, Typeface.BOLD_ITALIC);
            }
            if(arr[2][1].startsWith("*"))
            {
                arr[2][1] = arr[2][1].substring(1);
                tv3.setTypeface(Typeface.SERIF, Typeface.BOLD_ITALIC);
            }
            if(arr[3][1].startsWith("*"))
            {
                arr[3][1] = arr[3][1].substring(1);
                tv4.setTypeface(Typeface.SERIF, Typeface.BOLD_ITALIC);
            }

            // Set the text
            tv1.setText(arr[0][1]);
            tv2.setText(arr[1][1]);
            tv3.setText(arr[2][1]);
            tv4.setText(arr[3][1]);
        }

    }

    public void checkAnswer(View view) {
        if(alr) {
            return;
        }
        alr = true;

        // set correct one
        ColorDrawable[] corrColor = {new ColorDrawable(getResources().getColor(R.color.colorDaily)), new ColorDrawable(getResources().getColor(R.color.colorCorrect))};
        TransitionDrawable corrTrans = new TransitionDrawable(corrColor);
        ColorDrawable[] incorrColor = {new ColorDrawable(getResources().getColor(R.color.colorDaily)), new ColorDrawable(getResources().getColor(R.color.colorDaily_Incorrect))};
        TransitionDrawable incorrTrans = new TransitionDrawable(incorrColor);

        // set variables
        LinearLayout wrongLL;
        Toast pointToast;
        LinearLayout toastLayout;
        TextView toastTV;
        SharedPreferences prefs;
        SharedPreferences.Editor editor;
        int points;
        MenuItem settingsMenuItem;

        prefs = getSharedPreferences("score", MODE_PRIVATE);
        int games = prefs.getInt("daily_totalGames", 0);
        games++;
        int done = prefs.getInt("daily_done", 0);
        done++;

        prefs = getSharedPreferences("score", MODE_PRIVATE);
        editor = prefs.edit();
        editor.putInt("daily_totalGames", games);
        editor.putInt("daily_done", done);
        editor.commit();


        Resources res = this.getResources();
        int resID = res.getIdentifier("linearAnswer" + valueOf(iCorr+1), "id", this.getPackageName());
        LinearLayout corrLL = (LinearLayout) findViewById(resID);
        corrLL.setBackground(corrTrans);

        // do the checking
        int corrID = res.getIdentifier("answer" + valueOf(iCorr+1), "id", this.getPackageName());

        if(view.getId() == corrID)
        {
            corrTrans.startTransition(50);

            pointToast = Toast.makeText(this, "+10P", Toast.LENGTH_SHORT);
            toastLayout = (LinearLayout) pointToast.getView();
            toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(17);
            toastTV.setTypeface(null, Typeface.BOLD);
            pointToast.setGravity(Gravity.TOP|Gravity.RIGHT,15,15);
            pointToast.show();

            // add score
            prefs = getSharedPreferences("score", MODE_PRIVATE);
            points = prefs.getInt("daily_score", 0);
            points = points + 10;

            prefs = getSharedPreferences("score", MODE_PRIVATE);
            editor = prefs.edit();
            editor.putInt("daily_score", points);
            editor.commit();
        }
        else if(view.getId() == R.id.answer1){
            wrongLL = (LinearLayout)findViewById(R.id.linearAnswer1);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }
        else if(view.getId() == R.id.answer2){
            wrongLL = (LinearLayout)findViewById(R.id.linearAnswer2);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }
        else if(view.getId() == R.id.answer3){
            wrongLL = (LinearLayout)findViewById(R.id.linearAnswer3);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }
        else if(view.getId() == R.id.answer4){
            wrongLL = (LinearLayout)findViewById(R.id.linearAnswer4);
            wrongLL.setBackground(incorrTrans);
            incorrTrans.startTransition(50);
            corrTrans.startTransition(250);
        }



        settingsMenuItem = mainMenu.findItem(R.id.game_score);
        settingsMenuItem.setTitle(valueOf(done) + "/10");

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                TextView reset_question = (TextView)findViewById(R.id.question_daily);
                reset_question.setText("");
                reset_question = (TextView)findViewById(R.id.question_facts);
                reset_question.setText("");
                ImageView imageView = (ImageView) findViewById(R.id.daily_ImageView);
                imageView.setVisibility(View.INVISIBLE);

                LinearLayout reset = (LinearLayout)findViewById(R.id.linearAnswer1);
                reset.setBackgroundColor(getResources().getColor(R.color.colorDaily));
                reset = (LinearLayout)findViewById(R.id.linearAnswer2);
                reset.setBackgroundColor(getResources().getColor(R.color.colorDaily));
                reset = (LinearLayout)findViewById(R.id.linearAnswer3);
                reset.setBackgroundColor(getResources().getColor(R.color.colorDaily));
                reset = (LinearLayout)findViewById(R.id.linearAnswer4);
                reset.setBackgroundColor(getResources().getColor(R.color.colorDaily));
                alr = false;
                iDaily = rand.nextInt(4);
                readTask();
            }
        }, 1200);
    }

    public void setOnClicks()
    {
        View allViews = findViewById(R.id.answer1);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        allViews = findViewById(R.id.answer2);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        allViews = findViewById(R.id.answer3);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
        allViews = findViewById(R.id.answer4);
        allViews.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkAnswer(v);
            }
        });
    }

}
